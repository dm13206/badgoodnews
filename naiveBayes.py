import nltk
import time
from nltk.tokenize import word_tokenize

f_train_pos = open("news_titles/training_good.txt", "r", encoding='utf-8', errors='ignore')
f_train_neg = open("news_titles/training_bad.txt", "r", encoding='utf-8', errors='ignore')

news = []
for line in f_train_neg.readlines():
    x = (line.rstrip("\n"), " bad news")
    news.append(x)
for line in f_train_pos.readlines():
    x = (line.rstrip("\n"), " good news")
    news.append(x)

train = news

dictionary = set(word.lower() for passage in train for word in word_tokenize(passage[0]))

t = [({word: (word in word_tokenize(x[0])) for word in dictionary}, x[1]) for x in train]

classifier = nltk.NaiveBayesClassifier.train(t)


def bay():
    f_temp = open("news_titles/temp.txt", "r", encoding='utf-8', errors='ignore')
    linia = f_temp.readline()

    test_data = linia.rstrip("\n")
    test_data_features = {word.lower(): (word in word_tokenize(test_data.lower())) for word in dictionary}
    time.sleep(1)
    print("Checking with Naive Bayes...")
    time.sleep(1)
    print("Naive Bayes classified \"" + test_data + "\" as " + classifier.classify(test_data_features))
    print()
