f_pos = open("news_titles/good_news.txt", "r", encoding='utf-8',
              errors='ignore')
f_neg = open("news_titles/bad_news.txt", "r", encoding='utf-8',
              errors='ignore')


def positive_news():
    line = f_pos.readline()
    print(line)
    with open("news_titles/temp.txt", "w", encoding='utf-8',
              errors='ignore') as f_temp:
                    f_temp.write(line)


def negative_news():
    line = f_neg.readline()
    print(line)
    with open("news_titles/temp.txt", "w", encoding='utf-8',
              errors='ignore') as f_temp:
                    f_temp.write(line)


