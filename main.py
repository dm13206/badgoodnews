import newsPrint
import naiveBayes


def choose_news():
    choice = input('What kind of news would you like to hear? (B - bad news, G - good news) ')
    if choice == 'B':
        print("Bad news:")
        newsPrint.negative_news()
        naiveBayes.bay()
    elif choice == 'G':
        print("Good news:")
        newsPrint.positive_news()
        naiveBayes.bay()
    return choice


def user_input():
    x = ""
    while x != "Q":
        choose_news()


user_input()



