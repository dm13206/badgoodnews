import requests
import json
import time
from nltk.sentiment.vader import SentimentIntensityAnalyzer as SIA

hdr = {'User-Agent': 'windows:r/news.single.result:v1.0' +
       '(by /u/tenshiak)'}
url = 'https://www.reddit.com/r/news/new/.json'
req = requests.get(url, headers=hdr)
json_data = json.loads(req.text)

data_all = json_data['data']['children']
num_of_posts = 0
while len(data_all) <= 200:
    time.sleep(2)
    last = data_all[-1]['data']['name']
    url = 'https://www.reddit.com/r/news/new/.json?after=' + str(last)
    req = requests.get(url, headers=hdr)
    data = json.loads(req.text)
    data_all += data['data']['children']
    if num_of_posts == len(data_all):
        break
    else:
        num_of_posts = len(data_all)

sia = SIA()
pos_list = []
neg_list = []

for post in data_all:
    res = sia.polarity_scores(post['data']['title'])

    print(res)
    if res['compound'] > 0.25:
        pos_list.append(post['data']['title'])
    elif res['compound'] < -0.25:
        neg_list.append(post['data']['title'])

with open("news_titles/good_news.txt", "w", encoding='utf-8',
          errors='ignore') as f_pos:
    for post in pos_list:
        f_pos.write(post + "\n")

with open("news_titles/bad_news.txt", "w", encoding='utf-8',
          errors='ignore') as f_neg:
    for post in neg_list:
        f_neg.write(post + "\n")



